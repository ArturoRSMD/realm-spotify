package com.arturorsmd.realm;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class SongRealmHelper {

    public Realm realm;

    public SongRealmHelper(Realm realm) {
        this.realm = realm;
    }

    public static void createSong(String name, String artist){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Song song = new Song(name, artist);
        realm.copyToRealm(song);
        realm.commitTransaction();

    }

    public static void deleteDatabase(){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<Song> songRealmResults = realm.where(Song.class).findAll();
        songRealmResults.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public static List<Song> getSongsList(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Song> songRealmResults = realm.where(Song.class).findAll();
        ArrayList<Song> songArrayList = new ArrayList<>();
        for (Song s: songRealmResults){
            songArrayList.add(s);
        }

        return songArrayList;
    }

    public static List<Song> getSongListSpecial(String ll){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Song> realmResults = realm.where(Song.class).equalTo("name", ll).findAll();
        ArrayList<Song> songArrayList = new ArrayList<>();
        for (Song s: realmResults){
            songArrayList.add(s);
        }

        return songArrayList;
    }

    public static  void deleteSong(int id){

        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        RealmResults<Song> realmResults = realm.where(Song.class).equalTo("id", id).findAll();
        realmResults.deleteAllFromRealm();
        realm.commitTransaction();

    }

    public static void updateNameAndArtist(int id, String name, String artist){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        realm.commitTransaction();
    }

    public static void updateNameSong(int id, String name){

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Song subscription = realm.where(Song.class).equalTo("id", id).findFirst();
        subscription.setName(name);
        realm.commitTransaction();

    }

    public static void updateArtistSong(int id, String artist){

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Song subscription = realm.where(Song.class).equalTo("id", id).findFirst();
        subscription.setArtist(artist);
        realm.commitTransaction();

    }
}
