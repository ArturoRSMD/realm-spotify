package com.arturorsmd.realm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    LinearLayoutManager linearLayoutManager;
    Button btnAddSong;
    EditText edtxtName;
    EditText edtxtArtist;
    SongAdapter songAdapter;
    ImageView imgCoverPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final RecyclerView recyclerViewSong =  findViewById(R.id.rc_view_songs);
        btnAddSong = findViewById(R.id.btn_create);
        edtxtName = findViewById(R.id.edtxt_name);
        edtxtArtist = findViewById(R.id.edtxt_artist);
        imgCoverPage = findViewById(R.id.img_cover_page);


        linearLayoutManager = new LinearLayoutManager(this);

        recyclerViewSong.setLayoutManager(linearLayoutManager);
        recyclerViewSong.setHasFixedSize(true);
        songAdapter = new SongAdapter(this, SongRealmHelper.getSongsList());
        recyclerViewSong.setAdapter(songAdapter);

        imgCoverPage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                SongRealmHelper.deleteDatabase();

                songAdapter = new SongAdapter(getBaseContext(), SongRealmHelper.getSongsList());
                recyclerViewSong.setAdapter(songAdapter);

                return false;


            }
        });

        btnAddSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SongRealmHelper.createSong(edtxtName.getText().toString(), edtxtArtist.getText().toString());
                songAdapter = new SongAdapter(getBaseContext(), SongRealmHelper.getSongsList());
                recyclerViewSong.setAdapter(songAdapter);

            }
        });





    }




}
