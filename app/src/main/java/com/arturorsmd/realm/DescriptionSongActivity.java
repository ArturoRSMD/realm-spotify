package com.arturorsmd.realm;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DescriptionSongActivity extends AppCompatActivity {

    TextView txtName;
    TextView txtArtist;
    Button buttonDelete;



    EditText edtxtName;
    EditText edtxtArtist;
    LinearLayout llEdtxt;
    LinearLayout llTxt;
    Button btnSave;
    Button btnCancel;
    Button btnEditData;
    RelativeLayout btnsEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_song);

        txtName = findViewById(R.id.txt_description_name);
        txtArtist = findViewById(R.id.txt_description_artist);
        buttonDelete = findViewById(R.id.btn_delete);
        edtxtName = findViewById(R.id.edtxt_edit_name);
        edtxtArtist = findViewById(R.id.edtxt_edit_artist);
        llEdtxt = findViewById(R.id.ll_editext);
        llTxt = findViewById(R.id.ll_txt);
        btnSave = findViewById(R.id.btn_save_data);
        btnCancel = findViewById(R.id.btn_cancel_data);
        btnEditData = findViewById(R.id.btn_update_data);
        btnsEdit = findViewById(R.id.btns_edit);


        Bundle bundle = getIntent().getExtras();
        final String name = bundle.getString("name");
        final String artist = bundle.getString("artist");
        final int id = bundle.getInt("id");

        txtName.setText(name);
        txtArtist.setText(artist);

        btnEditData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llTxt.setVisibility(View.GONE);
                llEdtxt.setVisibility(View.VISIBLE);
                buttonDelete.setVisibility(View.GONE);
                btnEditData.setVisibility(View.GONE);
                btnsEdit.setVisibility(View.VISIBLE);

                edtxtName.setHint(name);
                edtxtArtist.setHint(artist);

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llTxt.setVisibility(View.VISIBLE);
                llEdtxt.setVisibility(View.GONE);
                buttonDelete.setVisibility(View.VISIBLE);
                btnEditData.setVisibility(View.VISIBLE);
                btnsEdit.setVisibility(View.GONE);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtxtName.getText().toString().equals("") && !edtxtArtist.getText().toString().equals("")){
                    SongRealmHelper.updateNameSong(id, edtxtName.getText().toString());
                    SongRealmHelper.updateArtistSong(id, edtxtArtist.getText().toString());

                    Intent intent = new Intent(DescriptionSongActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else if (!edtxtName.getText().toString().equals("") && edtxtArtist.getText().toString().equals("")){
                    SongRealmHelper.updateNameSong(id, edtxtName.getText().toString());
                    Intent intent = new Intent(DescriptionSongActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else if (edtxtName.getText().toString().equals("") && !edtxtArtist.getText().toString().equals("")){
                    SongRealmHelper.updateArtistSong(id, edtxtArtist.getText().toString());
                    Intent intent = new Intent(DescriptionSongActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }

            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SongRealmHelper.deleteSong(id);
                Intent intent = new Intent(DescriptionSongActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }
}
