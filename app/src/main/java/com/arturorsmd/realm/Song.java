package com.arturorsmd.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Song extends RealmObject {

    @PrimaryKey
    int id;
    @Required
    String name;
    @Required
    String artist;

    public Song(){

    }


    public Song(String name, String artist) {
        this.id = BaseRealmApplication.SONGID.incrementAndGet();
        this.name = name;
        this.artist = artist;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
}
