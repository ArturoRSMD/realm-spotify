package com.arturorsmd.realm;

import android.app.Application;

import java.util.concurrent.atomic.AtomicInteger;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class BaseRealmApplication extends Application {

    public static AtomicInteger SONGID = new AtomicInteger();

    @Override
    public void onCreate() {
        super.onCreate();

        realmInitSetup();
        realmIdSetup();

    }

    public void realmInitSetup(){
        Realm.init(this);
        RealmConfiguration realmSetup = new RealmConfiguration.Builder()
                .name("Database Name")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmSetup);
    }

    public void realmIdSetup(){
        Realm realm = Realm.getDefaultInstance();
        SONGID = getIdByTable(realm, Song.class);
        realm.close();
    }

    public void jdjd(){
        Realm realm = Realm.getDefaultInstance();
    }

    private <T extends RealmObject> AtomicInteger getIdByTable(Realm realm, Class<T> anyClass) {
        RealmResults<T> results = realm.where(anyClass).findAll();
        return (results.size() > 0) ? new AtomicInteger(results.max("id").intValue()) : new AtomicInteger();
    }

}
